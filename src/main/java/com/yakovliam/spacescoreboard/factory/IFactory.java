package com.yakovliam.spacescoreboard.factory;

public interface IFactory<T, I> {

    /**
     * Builds a T from an I
     *
     * @param input The input
     * @return The result
     */
    T build(I input);
}
