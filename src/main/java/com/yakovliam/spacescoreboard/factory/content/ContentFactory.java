package com.yakovliam.spacescoreboard.factory.content;

import com.yakovliam.spacescoreboard.factory.IFactory;
import com.yakovliam.spacescoreboard.factory.body.BodyFactory;
import com.yakovliam.spacescoreboard.factory.part.PartFactory;
import com.yakovliam.spacescoreboard.model.Body;
import com.yakovliam.spacescoreboard.model.Content;
import com.yakovliam.spacescoreboard.model.line.Line;
import org.bukkit.configuration.ConfigurationSection;

public class ContentFactory implements IFactory<Content, ConfigurationSection> {

    /**
     * Builds a T from an I
     *
     * @param input The input
     * @return The result
     */
    @Override
    public Content build(ConfigurationSection input) {
        // create content object
        Content content = new Content();

        // build title first
        Line title = new PartFactory().build(input.getConfigurationSection("title"));

        // set title
        content.setTitle(title);

        // build body
        Body body = new BodyFactory().build(input.getConfigurationSection("body"));
        // set body
        content.setBody(body);

        // return content (built)
        return content;
    }
}
