package com.yakovliam.spacescoreboard.factory.part;

import com.yakovliam.spacescoreboard.factory.part.line.FadingLineFactory;
import com.yakovliam.spacescoreboard.factory.part.line.RegularLineFactory;
import com.yakovliam.spacescoreboard.factory.part.line.RepeatingLineFactory;
import com.yakovliam.spacescoreboard.model.line.Line;
import org.bukkit.configuration.ConfigurationSection;

public class PartFactory implements IPartFactory<Line> {

    /**
     * Builds a T from an I
     *
     * @param input The input
     * @return The result
     */
    @Override
    public Line build(ConfigurationSection input) {
        // default regular
        IPartFactory<Line> partFactory = new RegularLineFactory();

        // get type from part section
        String type = input.getString("type");

        if (type != null) {
            //todo document different kinds of types of lines

            // if fading
            if (type.equalsIgnoreCase("fading") || type.equalsIgnoreCase("fade"))
                partFactory = new FadingLineFactory();

            // if repeating
            if(type.equalsIgnoreCase("repeating") || type.equalsIgnoreCase("repeat"))
                partFactory = new RepeatingLineFactory();
        }

        return partFactory.build(input);
    }
}
