package com.yakovliam.spacescoreboard.factory.part.line;

import com.yakovliam.spacescoreboard.factory.part.IPartFactory;
import com.yakovliam.spacescoreboard.model.line.Line;
import com.yakovliam.spacescoreboard.model.line.RepeatingLine;
import org.bukkit.configuration.ConfigurationSection;

public class RepeatingLineFactory implements IPartFactory<Line> {

    /**
     * Builds a T from an I
     *
     * @param input The input
     * @return The result
     */
    @Override
    public RepeatingLine build(ConfigurationSection input) {
        // easy factory :) Using regular values from the config.... :) (again, another 2 smiles :))
        return new RepeatingLine(input.getName(), input.getInt("interval"), input.getStringList("frames"), input.getInt("repeatsForTicks"));    }
}
