package com.yakovliam.spacescoreboard.factory.part;

import com.yakovliam.spacescoreboard.factory.IFactory;
import org.bukkit.configuration.ConfigurationSection;

public interface IPartFactory<T> extends IFactory<T, ConfigurationSection> {

    /**
     * Builds a T from an I
     *
     * @param input The input
     * @return The result
     */
    @Override
    T build(ConfigurationSection input);
}
