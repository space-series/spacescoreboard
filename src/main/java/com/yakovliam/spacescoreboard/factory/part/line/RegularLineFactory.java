package com.yakovliam.spacescoreboard.factory.part.line;

import com.yakovliam.spacescoreboard.factory.part.IPartFactory;
import com.yakovliam.spacescoreboard.model.line.Line;
import com.yakovliam.spacescoreboard.model.line.RegularLine;
import org.bukkit.configuration.ConfigurationSection;

public class RegularLineFactory implements IPartFactory<Line> {

    /**
     * Builds a T from an I
     *
     * @param input The input
     * @return The result
     */
    @Override
    public RegularLine build(ConfigurationSection input) {
        // easy factory, just return the two keys nested in the config section
        return new RegularLine(input.getName(), input.getInt("interval"), input.getStringList("frames"));
    }
}
