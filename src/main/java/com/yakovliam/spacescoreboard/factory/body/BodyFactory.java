package com.yakovliam.spacescoreboard.factory.body;

import com.yakovliam.spacescoreboard.factory.IFactory;
import com.yakovliam.spacescoreboard.factory.part.IPartFactory;
import com.yakovliam.spacescoreboard.factory.part.PartFactory;
import com.yakovliam.spacescoreboard.factory.part.line.FadingLineFactory;
import com.yakovliam.spacescoreboard.factory.part.line.RegularLineFactory;
import com.yakovliam.spacescoreboard.factory.part.line.RepeatingLineFactory;
import com.yakovliam.spacescoreboard.model.Body;
import com.yakovliam.spacescoreboard.model.line.Line;
import org.bukkit.configuration.ConfigurationSection;

import java.util.ArrayList;
import java.util.List;

public class BodyFactory implements IFactory<Body, ConfigurationSection> {

    /**
     * Builds a T from an I
     *
     * @param input The input
     * @return The result
     */
    @Override
    public Body build(ConfigurationSection input) {
        // create body object
        Body body = new Body();
        // create part list
        List<Line> bodyLines = new ArrayList<>();

        // loop through all keys in the body
        for (String line : input.getKeys(false)) {
            // get the part configuration section
            Line built = new PartFactory().build(input.getConfigurationSection(line));

            // add part to bodyLines
            bodyLines.add(built);
        }

        // set body (line)
        body.setLines(bodyLines);

        // return body (built)
        return body;
    }
}
