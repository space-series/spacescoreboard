package com.yakovliam.spacescoreboard.factory.criteria;

import com.yakovliam.spacescoreboard.factory.IFactory;
import com.yakovliam.spacescoreboard.model.Criteria;
import org.bukkit.configuration.ConfigurationSection;

public class CriteriaFactory implements IFactory<Criteria, ConfigurationSection> {

    /**
     * Builds a T from an I
     *
     * @param input The input
     * @return The result
     */
    @Override
    public Criteria build(ConfigurationSection input) {

        // create criteria
        Criteria criteria = new Criteria();

        // set isDefault
        criteria.setIsDefault(input.getBoolean("default"));

        // set permission
        criteria.setPermission(input.getString("permission"));

        // return criteria (built)
        return criteria;
    }
}
