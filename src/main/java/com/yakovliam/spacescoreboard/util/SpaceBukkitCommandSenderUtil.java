package com.yakovliam.spacescoreboard.util;

import com.yakovliam.spaceapi.command.SpaceCommandSender;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class SpaceBukkitCommandSenderUtil {

    /**
     * Gets a player from a SpaceCommandSender
     *
     * @param spaceCommandSender The command sender
     */
    public static Player getPlayer(SpaceCommandSender spaceCommandSender) {
        if (!spaceCommandSender.isPlayer()) return null;
        return Bukkit.getPlayer(spaceCommandSender.getUuid());
    }
}
