package com.yakovliam.spacescoreboard.util;

import com.yakovliam.spaceapi.confignew.impl.Configuration;
import com.yakovliam.spaceapi.text.Message;
import com.yakovliam.spacescoreboard.SpaceScoreboard;

public class Messages {

    // general
    public static final Message GENERAL_HELP = Message.fromConfigurationSection(getLangConfiguration().getSection("general.help"), "general.help")
            .build();

    // reload
    public static final Message RELOAD_SUCCESS = Message.fromConfigurationSection(getLangConfiguration().getSection("reload.success"), "reload.success")
            .build();
    public static final Message RELOAD_FAILURE = Message.fromConfigurationSection(getLangConfiguration().getSection("reload.failure"), "reload.failure")
            .build();

    // toggle
    public static final Message TOGGLED_ON = Message.fromConfigurationSection(getLangConfiguration().getSection("toggled.on"), "toggled.on")
            .build();
    public static final Message TOGGLED_OFF = Message.fromConfigurationSection(getLangConfiguration().getSection("toggled.off"), "toggled.off")
            .build();

    /**
     * Gets the lang configuration from the main class
     *
     * @return The lang configuration
     */
    private static Configuration getLangConfiguration() {
        return SpaceScoreboard.getInstance().getLangConfig().getConfig();
    }
}
