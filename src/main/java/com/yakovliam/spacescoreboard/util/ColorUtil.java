package com.yakovliam.spacescoreboard.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ColorUtil {

    private static final Pattern pattern = Pattern.compile("(?i)([&\u00A7][0-9a-fiklmno])+"); // match &<any> (that is a valid color code!)

    /**
     * Gets the nearest to last color code in a color
     *
     * @param text the text
     * @return The color code (e.g. &bBlue&cRed returns &c)
     */
    public static String getLastColor(String text) {
        Matcher matcher = pattern.matcher(text);

        String matched = "";

        while (matcher.find()) {
            matched = matcher.group();
        }

        return matched;
    }
}
