package com.yakovliam.spacescoreboard.commands;

import com.yakovliam.spaceapi.command.Command;
import com.yakovliam.spaceapi.command.Permissible;
import com.yakovliam.spaceapi.command.SpaceCommandSender;
import com.yakovliam.spacescoreboard.SpaceScoreboard;
import com.yakovliam.spacescoreboard.scoreboard.ScoreboardFactory;
import com.yakovliam.spacescoreboard.scoreboard.ScoreboardManager;
import com.yakovliam.spacescoreboard.update.ScoreboardUpdater;
import com.yakovliam.spacescoreboard.user.User;
import com.yakovliam.spacescoreboard.user.UserManager;
import lombok.SneakyThrows;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import static com.yakovliam.spacescoreboard.util.Messages.RELOAD_FAILURE;
import static com.yakovliam.spacescoreboard.util.Messages.RELOAD_SUCCESS;

@Permissible("space.scoreboard.reload")
public class ReloadCommand extends Command {

    public ReloadCommand() {
        super(SpaceScoreboard.getInstance().getAbstractPlugin(), "reload");
    }

    @SneakyThrows
    @Override
    public void onCommand(SpaceCommandSender spaceCommandSender, String s, String... args) {
        // create completable future async task
        CompletableFuture.runAsync(() -> {

            // reload configurations
            SpaceScoreboard.getInstance().loadConfigs();

            // re-build all scoreboards
            SpaceScoreboard.getInstance().setScoreboardManager(new ScoreboardManager());

            // get a list of all cached users
            List<User> userList = UserManager.getAll();

            // try catch
            try {
                // loop through users
                for (User user : userList) {
                    // cancel their current update task
                    user.getScoreboardUpdater().cancel();

                    // use the scoreboardFactory to build a new scoreboard (and set it!)
                    user.setScoreboard(ScoreboardFactory.getInstance().build(user.getPlayer()));

                    // create / draw new scoreboard by setting new updater
                    user.setScoreboardUpdater(new ScoreboardUpdater(user));
                }

            } catch (Exception e) {
                e.printStackTrace();
                // send failure message
                RELOAD_FAILURE.msg(spaceCommandSender);
                return;
            }

            // send success message
            RELOAD_SUCCESS.msg(spaceCommandSender);
        });
    }
}
