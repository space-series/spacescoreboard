package com.yakovliam.spacescoreboard.commands;

import com.yakovliam.spaceapi.command.*;
import com.yakovliam.spacescoreboard.SpaceScoreboard;
import com.yakovliam.spacescoreboard.user.User;
import com.yakovliam.spacescoreboard.user.UserManager;
import com.yakovliam.spacescoreboard.util.SpaceBukkitCommandSenderUtil;
import org.bukkit.entity.Player;

import static com.yakovliam.spacescoreboard.util.Messages.TOGGLED_OFF;
import static com.yakovliam.spacescoreboard.util.Messages.TOGGLED_ON;

@PlayersOnly
@Permissible("space.scoreboard.toggle")
public class ToggleCommand extends Command {

    public ToggleCommand() {
        super(SpaceScoreboard.getInstance().getAbstractPlugin(), "toggle");
    }

    @Override
    public void onCommand(SpaceCommandSender spaceCommandSender, String s, String... args) {
        // get player
        Player player = SpaceBukkitCommandSenderUtil.getPlayer(spaceCommandSender);

        // get user
        User user = UserManager.get(player.getUniqueId());

        // toggle player's scoreboard
        boolean newState = user.getScoreboardUpdater().toggle();

        // if toggled on
        if (newState)
            TOGGLED_ON.msg(spaceCommandSender);
        else // if toggled off
            TOGGLED_OFF.msg(spaceCommandSender);
    }
}
