package com.yakovliam.spacescoreboard.commands;

import com.yakovliam.spaceapi.command.Command;
import com.yakovliam.spaceapi.command.Permissible;
import com.yakovliam.spaceapi.command.SpaceCommandSender;
import com.yakovliam.spacescoreboard.SpaceScoreboard;

import static com.yakovliam.spacescoreboard.util.Messages.GENERAL_HELP;

@Permissible("space.scoreboard")
public class SpaceScoreboardCommand extends Command {

    public SpaceScoreboardCommand() {
        super(SpaceScoreboard.getInstance().getAbstractPlugin(), "spacescoreboard");

        // add sub commands
        addSubCommands(
                new ReloadCommand(),
                new ToggleCommand()
        );
    }

    @Override
    public void onCommand(SpaceCommandSender spaceCommandSender, String s, String... strings) {
        GENERAL_HELP.msg(spaceCommandSender);
    }
}
