package com.yakovliam.spacescoreboard.scoreboard;

import com.google.common.io.Files;
import com.yakovliam.spacescoreboard.SpaceScoreboard;
import com.yakovliam.spacescoreboard.factory.content.ContentFactory;
import com.yakovliam.spacescoreboard.factory.criteria.CriteriaFactory;
import com.yakovliam.spacescoreboard.model.Content;
import com.yakovliam.spacescoreboard.model.Criteria;
import com.yakovliam.spacescoreboard.model.Scoreboard;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.yakovliam.spacescoreboard.configuration.Config.DEFAULT_LOWEST_SCORE;

public class ScoreboardBuilder {

    /**
     * Builds all scoreboards from the config
     *
     * @return The list of built scoreboards
     */
    public List<Scoreboard> build() {
        // create list of scoreboards
        List<Scoreboard> scoreboardList = new ArrayList<>();

        // get a list of all files in the "scoreboards" directory
        List<File> scoreboardFiles = Arrays
                .stream(Objects.requireNonNull(new File(SpaceScoreboard.getInstance().getDataFolder() + File.separator + "scoreboards")
                        .listFiles()))
                .filter(file -> {
                    // get extension
                    String ext = Files.getFileExtension(file.getName());

                    return ext.equalsIgnoreCase("yml") || ext.equalsIgnoreCase("yaml");
                })
                .collect(Collectors.toList());

        // loop through all file contents to create scoreboards
        for (File scoreboardFile : scoreboardFiles) {
            // create scoreboard object
            Scoreboard scoreboard = new Scoreboard();

            // load as a configuration file
            FileConfiguration configuration = YamlConfiguration.loadConfiguration(scoreboardFile);

            /* go through contents and actually build */

            // set priority
            scoreboard.setPriority(configuration.getInt("priority"));

            // set handle
            scoreboard.setHandle(configuration.getString("handle"));

            // set lowest-score (and default provided)
            scoreboard.setLowestScore(configuration.getInt("lowest-score", DEFAULT_LOWEST_SCORE.get(SpaceScoreboard.getInstance()
                            .getSpaceScoreboardConfig()
                            .getConfig())));

            // build criteria
            Criteria criteria = new CriteriaFactory().build(configuration.getConfigurationSection("criteria"));
            // set criteria
            scoreboard.setCriteria(criteria);

            // build content
            Content content = new ContentFactory().build(configuration.getConfigurationSection("content"));
            // set content
            scoreboard.setContent(content);

            // add scoreboard to scoreboard list
            scoreboardList.add(scoreboard);
        }

        // return final build list
        return scoreboardList;
    }
}

