package com.yakovliam.spacescoreboard.scoreboard;

import com.yakovliam.spacescoreboard.SpaceScoreboard;
import com.yakovliam.spacescoreboard.model.Scoreboard;

import java.util.ArrayList;
import java.util.List;

public class ScoreboardManager {

    private List<Scoreboard> scoreboardList;

    public ScoreboardManager() {
        scoreboardList = new ArrayList<>();

        // get section that contains the scoreboard list

        // use scoreboardBuilder to build and return the list
        scoreboardList = SpaceScoreboard.getInstance().getScoreboardBuilder().build();
    }

    /**
     * Gets a scoreboard by the handle
     *
     * @param handle The handle of the scoreboard
     * @return The scoreboard that was found to have the handle provided
     */
    public Scoreboard getByHandle(String handle) {
        return scoreboardList
                .stream()
                .filter(scoreboard -> scoreboard.getHandle().equals(handle))
                .findFirst()
                .orElse(null);
    }

    /**
     * Gets all scoreboards
     *
     * @return The scoreboard list
     */
    public List<Scoreboard> getAll() {
        return scoreboardList;
    }
}
