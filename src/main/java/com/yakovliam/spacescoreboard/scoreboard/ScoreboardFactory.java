package com.yakovliam.spacescoreboard.scoreboard;

import com.yakovliam.spacescoreboard.SpaceScoreboard;
import com.yakovliam.spacescoreboard.factory.IFactory;
import com.yakovliam.spacescoreboard.model.Scoreboard;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.Comparator;
import java.util.Objects;

public class ScoreboardFactory implements IFactory<Scoreboard, OfflinePlayer> {

    /**
     * The scoreboard factory instance
     */
    private static ScoreboardFactory scoreboardFactory;

    /**
     * Gets the instance of the scoreboard factory
     *
     * @return The scoreboard factory
     */
    public static ScoreboardFactory getInstance() {
        if (scoreboardFactory == null)
            scoreboardFactory = new ScoreboardFactory();
        return scoreboardFactory;
    }

    /**
     * Builds a T from an I
     *
     * @param input The input
     * @return The result
     */
    @Override
    public Scoreboard build(OfflinePlayer input) {

        // if player null, return null
        if (input == null) return null;

        // if not online, return null
        if (!input.isOnline()) return null;

        // get player
        Player player = input.getPlayer();

        // get applicable scoreboard (by permission)
        return Objects.requireNonNull(SpaceScoreboard
                .getInstance()
                .getScoreboardManager()
                .getAll()
                .stream()
                .filter(scoreboard -> {
                            // if criteria null, return true
                            if (scoreboard.getCriteria() == null) return false;

                            // if default, return true
                            if (scoreboard.getCriteria().getIsDefault()) return true;

                            // if no permission, return false
                            if (scoreboard.getCriteria().getPermission() == null) return false;

                            // if  player has permission, return true (else, nothing matches so return false
                            return (player.hasPermission(scoreboard.getCriteria().getPermission()));
                        }
                ) // has permission for it (or default /shrug)
                .max(Comparator.comparing(Scoreboard::getPriority)) // highest priority
                .orElse(null)).clone();
    }
}
