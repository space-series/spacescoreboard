package com.yakovliam.spacescoreboard.configuration;

import com.yakovliam.spaceapi.confignew.keys.ConfigKeyTypes;
import com.yakovliam.spaceapi.confignew.keys.ConfigKey;
import com.yakovliam.spacescoreboard.SpaceScoreboard;

public class Config extends com.yakovliam.spaceapi.confignew.obj.Config {

    public Config() {
        super(SpaceScoreboard.getInstance().getAbstractPlugin(), "config.yml");
    }

    public static final ConfigKey<Integer> DEFAULT_LOWEST_SCORE = ConfigKeyTypes.integerKey("default-lowest-score", 1);
}
