package com.yakovliam.spacescoreboard.configuration;

import com.yakovliam.spaceapi.confignew.obj.Config;
import com.yakovliam.spacescoreboard.SpaceScoreboard;

import java.io.File;

public class DefaultScoreboardConfig extends Config {

    public DefaultScoreboardConfig() {
        super(SpaceScoreboard.getInstance().getAbstractPlugin(), new File(SpaceScoreboard.getInstance().getDataFolder() + File.separator + "scoreboards"), "default-scoreboard.yml");
    }
}
