package com.yakovliam.spacescoreboard.configuration;

import com.yakovliam.spaceapi.confignew.obj.Config;
import com.yakovliam.spacescoreboard.SpaceScoreboard;

public class LangConfig extends Config {

    public LangConfig() {
        super(SpaceScoreboard.getInstance().getAbstractPlugin(), "lang.yml");
    }
}
