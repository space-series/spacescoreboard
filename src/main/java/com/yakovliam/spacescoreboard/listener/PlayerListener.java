package com.yakovliam.spacescoreboard.listener;

import com.yakovliam.spacescoreboard.user.UserManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerListener implements Listener {

    /**
     * Fired when a player joins
     *
     * @param event The join event
     */
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        // load into cache
        UserManager.get(event.getPlayer().getUniqueId());
    }

    /**
     * Fired when a player leaves
     *
     * @param event The leave event
     */
    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent event) {
        // load into cache
        UserManager.invalidate(event.getPlayer().getUniqueId());
    }
}
