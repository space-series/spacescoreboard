package com.yakovliam.spacescoreboard.user;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.yakovliam.spacescoreboard.scoreboard.ScoreboardFactory;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserManager {

    /**
     * The scoreboard factory
     */
    private static LoadingCache<UUID, User> userCache = Caffeine.newBuilder()
            .build(key -> {
                // create new user
                return new User(key, ScoreboardFactory.getInstance()
                        .build(Bukkit.getOfflinePlayer(key)));
            });

    /**
     * Gets a user by UUID
     *
     * @param uuid The player's uuid
     * @return The user object
     */
    public static User get(UUID uuid) {
        return userCache.get(uuid);
    }

    /**
     * Invalidates a user
     *
     * @param uuid The player's uuid
     */
    public static void invalidate(UUID uuid) {
        // cancel scoreboard task
        get(uuid).getScoreboardUpdater().cancel();

        // invalidate
        userCache.invalidate(uuid);
    }

    /**
     * Gets all users
     *
     * @return The user list
     */
    public static List<User> getAll() {
        return new ArrayList<>(userCache.asMap().values());
    }
}
