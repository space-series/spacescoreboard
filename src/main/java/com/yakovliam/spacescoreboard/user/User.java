package com.yakovliam.spacescoreboard.user;

import com.yakovliam.spacescoreboard.model.Scoreboard;
import com.yakovliam.spacescoreboard.update.ScoreboardUpdater;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

@NoArgsConstructor
public class User {

    /**
     * The player's UUID
     */
    @Getter
    @Setter
    private UUID uuid;

    /**
     * The player's applicable scoreboard (object)
     */
    @Getter
    @Setter
    private Scoreboard scoreboard;

    /**
     * The updater for the player
     */
    @Getter
    @Setter
    private ScoreboardUpdater scoreboardUpdater;

    /**
     * Creates a new user
     *
     * @param uuid       The uuid of the player
     * @param scoreboard The scoreboards
     */
    public User(UUID uuid, Scoreboard scoreboard) {
        this.uuid = uuid;
        this.scoreboard = scoreboard;

        this.scoreboardUpdater = new ScoreboardUpdater(this);
    }

    /**
     * Gets the online player object from the uuid
     *
     * @return The player
     */
    public Player getPlayer() {
        return Bukkit.getPlayer(uuid);
    }
}
