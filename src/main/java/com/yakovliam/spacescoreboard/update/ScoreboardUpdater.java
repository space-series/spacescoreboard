package com.yakovliam.spacescoreboard.update;

import com.yakovliam.spacescoreboard.SpaceScoreboard;
import com.yakovliam.spacescoreboard.model.line.Line;
import com.yakovliam.spacescoreboard.user.User;
import com.yakovliam.spacescoreboard.util.ColorUtil;
import lombok.Getter;
import lombok.Setter;
import me.clip.placeholderapi.PlaceholderAPI;
import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class ScoreboardUpdater {

    private static final String OBJECTIVE_NAME = " ";

    /**
     * The applicable user
     */
    @Getter
    private final User user;

    /**
     * The bukkit scoreboard
     */
    private Scoreboard scoreboard;

    /**
     * The user scoreboard
     */
    private com.yakovliam.spacescoreboard.model.Scoreboard userScoreboard;

    /**
     * The list of lines (as teams)
     */
    private final Map<String, Team> lines;

    /**
     * The color random picker
     */
    @Getter
    private final RandomPicker randomPicker;

    /**
     * The update task ids
     */
    private final List<Integer> updateTaskIds;

    /**
     * If the scoreboard is toggled
     */
    @Setter
    @Getter
    private boolean toggled;

    /**
     * Creates new updater
     */
    public ScoreboardUpdater(User user) {
        this.user = user;
        this.lines = new HashMap<>();
        this.randomPicker = new RandomPicker();
        this.updateTaskIds = new ArrayList<>();

        // run 'create' on main thread
        Bukkit.getScheduler().runTask(SpaceScoreboard.getInstance(), this::create);
    }

    /**
     * Creates a new scoreboard
     */
    public void create() {
        // set toggled on
        toggled = true;

        // set userScoreboard
        userScoreboard = user.getScoreboard();

        // if null, throw error
        Validate.notNull(userScoreboard);

        // create new board
        scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();

        // set display
        Objective displayDummy = scoreboard.registerNewObjective(OBJECTIVE_NAME, OBJECTIVE_NAME);
        displayDummy.setDisplaySlot(DisplaySlot.SIDEBAR);

        // set scoreboard for player
        user.getPlayer().setScoreboard(scoreboard);

        AtomicInteger lineNum = new AtomicInteger(userScoreboard.getContent().getBody().getLines().size() - 1);

        // create lines
        userScoreboard
                .getContent()
                .getBody()
                .getLines()
                .forEach(line -> {
                    // create team
                    Team team = scoreboard.registerNewTeam(line.getHandle());

                    // set default prefix / suffix of ""
                    team.setPrefix("");
                    team.setSuffix("");

                    String randomColor = randomPicker.buildRandom();

                    // set unique entry for random color
                    team.addEntry(randomColor);

                    // set visible with score
                    displayDummy.getScore(randomColor).setScore(lineNum.get());

                    lines.put(line.getHandle(), team);

                    // subtract
                    lineNum.getAndDecrement();
                });

        /* START UPDATE TASKS */

        /* Title update task */

        // get title line
        Line title = userScoreboard.getContent().getTitle();

        // create title update task (and add to list of update task ids)
        updateTaskIds.add(Bukkit.getScheduler().runTaskTimerAsynchronously(SpaceScoreboard.getInstance(), () -> {
            // update title with text
            scoreboard.getObjective(OBJECTIVE_NAME)
                    .setDisplayName(
                            replace(
                                    userScoreboard.getContent().getTitle().next()
                            )
                    );
        }, 0L, title.getUpdateInterval()).getTaskId());

        /* All lines update tasks */

        // loop through each line (teams)
        lines.forEach((handle, team) -> {

            // get line by handle
            Line line = userScoreboard
                    .getContent()
                    .getBody()
                    .getLines()
                    .stream()
                    .filter(l -> l.getHandle().equals(handle))
                    .findFirst().orElse(null);

            // if no line, return
            if (line == null) return;

            // create a new update task for the current line (and add to list of currently running)
            updateTaskIds.add(Bukkit.getScheduler().runTaskTimerAsynchronously(SpaceScoreboard.getInstance(), () -> {
                String next = line.next();

                // next
                next = replace(next);

                // The text doesn't have more than 16 characters, so we are fine
                if (next.length() <= 16) {
                    team.setPrefix(next);
                    return;
                }

                // If the text actually goes above 32, cut it to 32 to prevent kicks and errors
                if (next.length() > 32) {
                    next = next.substring(0, 32);
                }

                String prefix = next.substring(0, 16);
                String suffix = next.substring(16);

                // Set the prefix to the first 16 characters
                team.setPrefix(prefix);

                // set last color in the beginning of the suffix because the entry has colors in it, which effectively resets it
                suffix = ColorUtil.getLastColor(prefix) + suffix;

                if (suffix.length() > 16)
                    suffix = suffix.substring(0, 16);

                // Now use the last 16 characters and put them into the suffix
                team.setSuffix(suffix);
            }, 0L, line.getUpdateInterval()).getTaskId());
        });
    }

// Reason for comment out: It uses polling (also not complete) which is not great for performance. It is better to create a separate update task for each line
//    /**
//     * Ticks the scoreboard
//     */
//    public void tick() {
//
//        // go through each line and retrieve the text for that line
//        lines.forEach((handle, team) -> {
//            // get line by handle
//            Line line = userScoreboard
//                    .getContent()
//                    .getBody()
//                    .getLines()
//                    .stream()
//                    .filter(l -> l.getHandle().equals(handle))
//                    .findFirst().orElse(null);
//
//            // if no line, return
//            if (line == null) return;
//
//            // next
//            String next = replace(line.next());
//
//            // The text doesn't have more than 16 characters, so we are fine
//            if (next.length() <= 16) {
//                team.setPrefix(next);
//                return;
//            }
//
//            // If the text actually goes above 32, cut it to 32 to prevent kicks and errors
//            if (next.length() > 32) {
//                next = next.substring(0, 32);
//            }
//
//            String prefix = next.substring(0, 16);
//            String suffix = next.substring(16);
//
//            // Set the prefix to the first 16 characters
//            team.setPrefix(prefix);
//
//            // set last color in the beginning of the suffix because the entry has colors in it, which effectively resets it
//            suffix = ColorUtil.getLastColor(prefix) + suffix;
//
//            if (suffix.length() > 16)
//                suffix = suffix.substring(0, 16);
//
//            // Now use the last 16 characters and put them into the suffix
//            team.setSuffix(suffix);
//        });
//    }

    private String replace(String s) {
        return PlaceholderAPI.setPlaceholders(user.getPlayer(), s, true);
    }

    /**
     * Cancels the update task
     */
    public void cancel() {

        Bukkit.getScheduler().runTask(SpaceScoreboard.getInstance(), () -> {
            // remove scoreboard by setting a new one
            user.getPlayer().setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
            // set toggled off
            toggled = false;
            // cancel all update tasks
            updateTaskIds.forEach(Bukkit.getScheduler()::cancelTask);
        });
    }

    /**
     * Toggles the scoreboard status
     *
     * @return The new state
     */
    public boolean toggle() {

        Bukkit.getScheduler().runTask(SpaceScoreboard.getInstance(), () -> {
            // set toggled to !
            toggled = !toggled;

            // if new state is toggle ON, cancel then create
            if (toggled) {
                cancel();
                create();
            } else { // if new state is toggle OFF, cancel
                cancel();
            }
        });

        return !toggled;
    }

}
