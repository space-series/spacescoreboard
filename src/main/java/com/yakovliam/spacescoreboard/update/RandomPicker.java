package com.yakovliam.spacescoreboard.update;

import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class RandomPicker {

    private ChatColor[] available = new ChatColor[]{
            ChatColor.AQUA,
            ChatColor.BLACK,
            ChatColor.BLUE,
            ChatColor.DARK_AQUA,
            ChatColor.DARK_BLUE,
            ChatColor.DARK_GRAY,
            ChatColor.DARK_GREEN,
            ChatColor.DARK_PURPLE,
            ChatColor.DARK_RED,
            ChatColor.GOLD,
            ChatColor.GRAY,
            ChatColor.GREEN,
            ChatColor.LIGHT_PURPLE,
            ChatColor.RED,
            ChatColor.WHITE,
            ChatColor.YELLOW,
            ChatColor.RESET,
    };

    private final List<String> alreadyChosen;

    public RandomPicker() {
        alreadyChosen = new ArrayList<>();
    }

    public String buildRandom() {
        boolean foundNew = false;

        String random = "";

        while (!foundNew) {
            // get random
            random = getRandom();

            // if already chosen, keep going. else, continue on
            if (!alreadyChosen.contains(random)) foundNew = true;
        }

        // add random to alreadyChosen list
        alreadyChosen.add(random);

        // return
        return random;
    }

    private String getRandom() {
        String random = "";

        /* get random from available */
        List<ChatColor> toPickFrom = Arrays.asList(available);

        // shuffle
        Collections.shuffle(toPickFrom);

        // get random
        ChatColor first = toPickFrom.get(0);
        // get random again
        ChatColor second = toPickFrom.get(1);

        // build random string
        random += first;
        random += "";
        random += second;

        // return
        return random;
    }
}
