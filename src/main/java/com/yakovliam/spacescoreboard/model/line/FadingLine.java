package com.yakovliam.spacescoreboard.model.line;

import java.util.ArrayList;
import java.util.List;

public class FadingLine extends Line {

    /**
     * Frames of the animation. This acts as a database
     * of different lines to show in succession instead of one per tick
     */
    private List<String> frames;

    /**
     * Creates new Line
     *
     * @param handle         The handle
     * @param updateInterval The update interval
     */
    public FadingLine(String handle, Integer updateInterval, List<String> frames) {
        super(handle, updateInterval);

        this.frames = frames;
    }

    /**
     * Gets the next frame
     *
     * @return The next frame
     */
    @Override
    public String next() {
        //todo
        return null;
    }

    /**
     * Clones the object
     *
     * @return The cloned object
     */
    @Override
    public Line clone() {
        return new FadingLine(this.handle, this.updateInterval, new ArrayList<>(this.frames));
    }
}
