package com.yakovliam.spacescoreboard.model.line;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class RegularLine extends Line {

    /**
     * The applicable text in frames
     */
    @Getter
    private List<String> frames;

    /**
     * Initializes new RegularLine
     *
     * @param handle         The handle
     * @param frames         The list of frames
     * @param updateInterval The update interval (in minecraft ticks)
     */
    public RegularLine(String handle, Integer updateInterval, List<String> frames) {
        super(handle, updateInterval);

        this.frames = frames;
    }

    /**
     * Gets the next frame
     *
     * @return The next frame
     */
    public String next() {
        // get the current frame
        String frame = frames.get(currentFrame);

        // increment
        currentFrame++;

        // if reached max, reset current frame
        if (currentFrame >= this.frames.size()) this.currentFrame = 0;

        // return
        return frame;
    }

    /**
     * Clones the object
     *
     * @return The cloned object
     */
    @Override
    public Line clone() {
        return new RegularLine(this.handle, this.updateInterval, new ArrayList<>(this.frames));
    }
}
