package com.yakovliam.spacescoreboard.model.line;

import lombok.Getter;

public abstract class Line implements Cloneable {

    /**
     * Error parsing string
     */
    public static final String ERROR = "&cError Parsing";

    /**
     * The handle of the line
     */
    @Getter
    public String handle;

    /**
     * The current frame
     */
    @Getter
    public int currentFrame;

    /**
     * The update interval of the part
     * This interval is in Minecraft Ticks (20 ticks = 1 second)
     */
    @Getter
    public Integer updateInterval;

    /**
     * Creates new Line
     *
     * @param updateInterval The update interval
     * @param handle The handle
     */
    public Line(String handle, Integer updateInterval) {
        this.handle = handle;
        this.updateInterval = updateInterval;
        this.currentFrame = 0;
    }

    /**
     * Gets the next frame
     *
     * @return The next frame
     */
    public abstract String next();

    /**
     * Clones the object
     *
     * @return The cloned object
     */
    public abstract Line clone();
}
