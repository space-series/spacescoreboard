package com.yakovliam.spacescoreboard.model.line;

import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class RepeatingLine extends Line {

    /**
     * How long (in minecraft ticks) the line stays for
     */
    private Integer repeatsForTicks;

    /**
     * The frames of the animations
     * This acts as '1 frame = 1 line (which repeats for x ticks)`
     */
    private List<String> frames;

    /**
     * The number of times shown per frame (calculated on instantiation of the repeating line)
     */
    private Integer timesShownPerFrame;

    /**
     * The number of times the current frame has been shown
     */
    private Integer currentTimesShown;

    /**
     * Creates new Line
     *
     * @param handle         The handle
     * @param updateInterval The update interval (in minecraft ticks)
     */
    public RepeatingLine(String handle, Integer updateInterval, List<String> frames, Integer repeatsForTicks) {
        super(handle, updateInterval);

        this.frames = frames;
        this.repeatsForTicks = repeatsForTicks;
        this.currentTimesShown = 0;

        // calculate timesShownPerFrame
        try {
            timesShownPerFrame = repeatsForTicks / updateInterval;
        } catch (Exception ignored) {
            Bukkit.getLogger().log(Level.SEVERE, "Error calculating timesShownPerFrame value for " + handle);
        }
    }

    /**
     * Gets the next frame
     *
     * @return The next frame
     */
    @Override
    public String next() {
        //TODO implement type of repeating frame shown in default-scoreboard.yml

        // if there was an error parsing, return the error string
        if (timesShownPerFrame == null) return ERROR;

        // get the current frame
        String frame = frames.get(currentFrame);
        // increment the number of times shown (for the current frame)
        currentTimesShown++;

        // if the number of times shown is greater than the number of times to show per frame, then increment current frame and reset current times shown
        if (currentTimesShown >= timesShownPerFrame) {
            // increment frame
            currentFrame++;
            // reset times shown
            currentTimesShown = 0;
        }

        // if reached max, reset current frame
        if (currentFrame >= this.frames.size()) this.currentFrame = 0;

        // return
        return frame;
    }

    /**
     * Clones the object
     *
     * @return The cloned object
     */
    @Override
    public Line clone() {
        return new RepeatingLine(this.handle, this.updateInterval, new ArrayList<>(this.frames), this.repeatsForTicks);
    }
}
