package com.yakovliam.spacescoreboard.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class Criteria {

    /**
     * The permission node required to use/see
     */
    @Getter
    @Setter
    private String permission;

    /**
     * Is the scoreboard default?
     */
    @Getter
    @Setter
    private Boolean isDefault;

    /**
     * Clones the object
     *
     * @return The cloned object
     */
    public Criteria clone() {
        return new Criteria(this.permission, this.isDefault);
    }

}
