package com.yakovliam.spacescoreboard.model;

import com.yakovliam.spacescoreboard.model.line.Line;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
public class Body implements Cloneable {

    /**
     * The list of parts in the scoreboard body
     */
    @Getter
    @Setter
    private List<Line> lines;

    /**
     * Clones the object
     *
     * @return The cloned object
     */
    public Body clone() {
        return new Body(this.lines.stream().map(Line::clone).collect(Collectors.toList()));
    }
}
