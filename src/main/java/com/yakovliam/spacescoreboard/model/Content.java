package com.yakovliam.spacescoreboard.model;

import com.yakovliam.spacescoreboard.model.line.Line;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class Content implements Cloneable {

    /**
     * The title of the board
     */
    @Getter
    @Setter
    private Line title;

    /**
     * The body of the scoreboard
     */
    @Getter
    @Setter
    private Body body;

    /**
     * Clones the object
     *
     * @return The cloned object
     */
    public Content clone() {
        return new Content(this.title.clone(), this.body.clone());
    }
}
