package com.yakovliam.spacescoreboard.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class Scoreboard implements Cloneable {

    /**
     * The priority of the scoreboard
     */
    @Getter
    @Setter
    private Integer priority;

    /**
     * The handle for the scoreboard
     */
    @Getter
    @Setter
    private String handle;

    /**
     * The lowest score on the scoreboard
     */
    @Getter
    @Setter
    private Integer lowestScore;

    /**
     * The scoreboard criteria
     */
    @Getter
    @Setter
    private Criteria criteria;

    /**
     * The scoreboard content
     */
    @Getter
    @Setter
    private Content content;

    /**
     * Clones the object
     *
     * @return The cloned object
     */
    public Scoreboard clone() {
        return new Scoreboard(this.priority, this.handle, this.lowestScore, this.criteria.clone(), this.content.clone());
    }
}
