package com.yakovliam.spacescoreboard;

import com.yakovliam.spaceapi.abstraction.plugin.BukkitPlugin;
import com.yakovliam.spaceapi.abstraction.plugin.Plugin;
import com.yakovliam.spacescoreboard.commands.SpaceScoreboardCommand;
import com.yakovliam.spacescoreboard.configuration.Config;
import com.yakovliam.spacescoreboard.configuration.DefaultScoreboardConfig;
import com.yakovliam.spacescoreboard.configuration.LangConfig;
import com.yakovliam.spacescoreboard.listener.PlayerListener;
import com.yakovliam.spacescoreboard.scoreboard.ScoreboardBuilder;
import com.yakovliam.spacescoreboard.scoreboard.ScoreboardManager;
import com.yakovliam.spacescoreboard.stats.MetricsHandler;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.plugin.java.JavaPlugin;

public final class SpaceScoreboard extends JavaPlugin {

    /**
     * The instance of the main class (this)
     */
    @Getter
    private static SpaceScoreboard instance;

    /**
     * The plugin's abstract plugin provided by the API
     */
    @Getter
    @Setter
    private Plugin abstractPlugin;

    /**
     * The main plugin config
     */
    @Getter
    private Config spaceScoreboardConfig;

    /**
     * The default scoreboard config file
     */
    @Getter
    private DefaultScoreboardConfig defaultScoreboardConfig;

    /**
     * Scoreboard builder
     */
    @Getter
    @Setter
    private ScoreboardBuilder scoreboardBuilder;

    /**
     * Scoreboard manager
     */
    @Getter
    @Setter
    private ScoreboardManager scoreboardManager;

    /**
     * The lang configuration
     */
    @Getter
    private LangConfig langConfig;

    @Override
    public void onLoad() {
        instance = this;
    }

    @Override
    public void onEnable() {
        // initialize api
        abstractPlugin = new BukkitPlugin(this);

        // Plugin startup logic
        loadConfigs();

        // initialize scoreboard builder
        scoreboardBuilder = new ScoreboardBuilder();

        // initialize scoreboard manager
        scoreboardManager = new ScoreboardManager();

        // initialize metrics
        new MetricsHandler();

        // register commands
        abstractPlugin.registerCommand(new SpaceScoreboardCommand());

        // register listeners
        this.getServer().getPluginManager().registerEvents(new PlayerListener(), this);
    }

    /**
     * Loads configs
     */
    public void loadConfigs() {
        // the main configuration
        spaceScoreboardConfig = new Config();
        // the default scoreboard configuration
        defaultScoreboardConfig = new DefaultScoreboardConfig();
        // the lang config
        langConfig = new LangConfig();
    }
}
