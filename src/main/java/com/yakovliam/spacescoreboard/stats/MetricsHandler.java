package com.yakovliam.spacescoreboard.stats;

import com.yakovliam.spacescoreboard.SpaceScoreboard;
import org.bstats.bukkit.Metrics;

public class MetricsHandler {

    public MetricsHandler() {
//        init();
    }

    private static final int pluginId = -1;

    public static void init() {
        // init metrics
        new Metrics(SpaceScoreboard.getInstance(), pluginId);
    }
}
